class Plansza:
    def __init__(self, gracz):
        self.plan = [["-", "-", "-"], ["-", "-", "-"], ["-", "-", "-"]]
        self.gracz = gracz
        self.win = False

    def printPlansza(self):
        print("  1 2 3")
        wersNum = 1
        for wers in self.plan:
            print(wersNum, end=" ")
            for element in wers:
                print(element, end=" ")
            print()
            wersNum += 1

    def putToBoard(self, x, y):
        if self.checkIfFree(x, y):
            self.plan[x - 1][y - 1] = self.gracz
            self.changeThePlayer()

    def checkIfFree(self, x, y):
        return self.plan[x - 1][y - 1] == "-"

    def getPlayer(self):
        return self.gracz

    def changeThePlayer(self):
        if self.gracz == "O":
            self.gracz = "X"
        else:
            self.gracz = "O"

    def returnBoard(self):
        return self.plan

    def checkIfDraw(self):
        if not self.checkIfWin():
            for wiersz in self.plan:
                for element in wiersz:
                    if element == "-":
                        return False
            return True
        else:
            return False

    def checkIfWin(self):
        for x in range(0, 3):
            if self.plan[x][0] == self.plan[x][1] and self.plan[x][1] == self.plan[x][2] and (
                    self.plan[x][2] == "X" or self.plan[x][2] == "O"):
                self.win = True
                return True
        for y in range(0, 3):
            if self.plan[0][y] == self.plan[1][y] and self.plan[1][y] == self.plan[2][y] and (
                    self.plan[2][y] == "X" or self.plan[2][y] == "O"):
                self.win = True
                return True
        if self.plan[0][0] == self.plan[1][1] and self.plan[1][1] == self.plan[2][2] and (
                self.plan[2][2] == "X" or self.plan[2][2] == "O"):
            self.win = True
            return True
        if self.plan[0][2] == self.plan[1][1] and self.plan[1][1] == self.plan[2][0] and (
                self.plan[2][0] == "X" or self.plan[2][0] == "O"):
            self.win = True
            return True

        return False














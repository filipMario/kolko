from sfml import sf
from Plansza import Plansza

# ROZGRYWKA ODBYWA SIĘ PRZY UŻYCIU NUM PADA!!! GRĘ PRZERYWAMY KLAWISZEM ESC.

window = sf.RenderWindow(sf.VideoMode(600,600),"Tick Tack")

textureX = sf.Texture.from_file('krzyz.png')
textureO = sf.Texture.from_file('kolko.png')
textureP = sf.Texture.from_file('brak.png')

tablicaX = []
for i in range(9):
    sprite = sf.Sprite(textureX)
    tablicaX.append(sprite)

tablicaO = []
for j in range(9):
    sprite = sf.Sprite(textureO)
    tablicaO.append(sprite)

tablicaB = []
for k in range(9):
    sprite = sf.Sprite(textureP)
    tablicaB.append(sprite)

objectBoard = Plansza("X")

mousePosition = sf.Vector2(-1,-1)

while window.is_open:
    window.clear(sf.Color.WHITE)

    event = sf.Event()
    board = objectBoard.returnBoard()
    if sf.Keyboard.is_key_pressed(sf.Keyboard.ESCAPE):
        window.close()

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD1)):
        objectBoard.putToBoard(3, 1)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD2)):
        objectBoard.putToBoard(3, 2)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD3)):
        objectBoard.putToBoard(3, 3)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD4)):
        objectBoard.putToBoard(2, 1)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD5)):
        objectBoard.putToBoard(2, 2)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD6)):
        objectBoard.putToBoard(2, 3)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD7)):
        objectBoard.putToBoard(1, 1)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD8)):
        objectBoard.putToBoard(1, 2)

    if (sf.Keyboard.is_key_pressed(sf.Keyboard.NUMPAD9)):
        objectBoard.putToBoard(1, 3)


    rectangle1 = sf.RectangleShape()
    rectangle1.size = (600,0)
    rectangle1.outline_color = sf.Color.GREEN
    rectangle1.outline_thickness = 5
    rectangle1.position = (0,200)

    rectangle2 = sf.RectangleShape()
    rectangle2.size = (600,0)
    rectangle2.outline_color = sf.Color.GREEN
    rectangle2.outline_thickness = 5
    rectangle2.position = (0,400)

    rectangle3 = sf.RectangleShape()
    rectangle3.size = (0,600)
    rectangle3.outline_color = sf.Color.GREEN
    rectangle3.outline_thickness = 5
    rectangle3.position = (200,0)

    rectangle4 = sf.RectangleShape()
    rectangle4.size = (0,600)
    rectangle4.outline_color = sf.Color.GREEN
    rectangle4.outline_thickness = 5
    rectangle4.position = (400,0)

    window.draw(rectangle1)
    window.draw(rectangle2)
    window.draw(rectangle3)
    window.draw(rectangle4)

    toDraw = []
    for aX in range(3):
        for aY in range(3):
            if board[aX][aY] == '-':
                tablicaB[3 * aX + aY].position = sf.Vector2(200 * aY, 200 * aX)
                toDraw.append(tablicaB[3 * aX + aY])
            elif board[aX][aY] == 'X':
                tablicaX[3 * aX + aY].position = sf.Vector2(200 * aY, 200 * aX)
                toDraw.append(tablicaX[3 * aX + aY])
            else:
                tablicaO[3 * aX + aY].position = sf.Vector2(200 * aY, 200 * aX)
                toDraw.append(tablicaO[3 * aX + aY])

    for elementToDraw in range(9):
        window.draw(toDraw[elementToDraw])

    window.display()

    if objectBoard.checkIfWin() or objectBoard.checkIfDraw():
        break

if objectBoard.checkIfDraw():
    print("REMIS")
else:
    if objectBoard.getPlayer() == 'O':
        print("Wygrywają krzyżyki")
    else:
        print("Wygrywają kółka")